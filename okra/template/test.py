# -*- coding: utf-8 -*-
from glob import glob
import os.path
import unittest
import okra.test

if __name__ == '__main__':
    project = os.path.dirname(__file__)
    target_file = os.path.join(project, 'main.py')

    input_files_pattern = os.path.join(project, 'input_*.txt')
    input_files = sorted(glob(input_files_pattern))

    expect_files_pattern = os.path.join(project, 'expect_*.txt')
    expect_files = sorted(glob(expect_files_pattern))

    suite = okra.test.create_suite(target_file, input_files, expect_files)
    unittest.TextTestRunner().run(suite)

# -*- coding: utf-8 -*-
import unittest
from subprocess import Popen, PIPE


def create_suite(target_file, input_files, expect_files):
    """テストスイートを作成
    """
    suite = unittest.TestSuite()
    testclass = _create_testclass(target_file, input_files, expect_files)
    suite.addTest(unittest.makeSuite(testclass))
    return suite


def _create_testclass(target_file, input_files, expect_files):
    """テストクラスを動的に生成
    """
    # 入力データファイルと期待する実行結果のファイルのタプルを生成
    files = zip(input_files, expect_files)

    # テストクラスを生成
    test_class = type('TestCase', (unittest.TestCase, ), {})
    # テストクラスにテストメソッドを追加する
    for i, (input_file, expect_file) in enumerate(files, 1):
        test_name = 'testMethod_{}'.format(i)
        testmethod = _create_testmethod(target_file, input_file, expect_file)
        setattr(test_class, test_name, testmethod)

    return test_class


def _create_testmethod(target_file, input_file, expect_file):
    """テストメソッドを動的に生成
    """
    def testmethod(self):
        # 入力データをパイプするプロセスを作成
        input_command = ['cat', input_file]
        input_data = Popen(input_command, stdout=PIPE)

        # テスト対象ファイルを実行するプロセスを作成
        test_command = ['python2', target_file]
        output = Popen(test_command, stdin=input_data.stdout, stdout=PIPE)
        # テスト対象ファイルの実行結果を取得する
        result, err = output.communicate()
        if type(result) == bytes:
            result = result.decode()

        # 期待する実行結果をファイルから読み込む
        expect_data = open(expect_file, 'r').read()

        # 実行結果と期待する実行結果を比較する
        self.assertEqual(result, expect_data)
    return testmethod

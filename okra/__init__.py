# -*- coding: utf-8 -*-
from .cli import cli
from . import test

__all__ = ['cli', 'test']

if __name__ == '__main__':
    cli()

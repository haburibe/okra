# -*- coding: utf-8 -*-
import os.path
import shutil
import subprocess
import sys
import click
import okra


@click.group()
def cli():
    pass


@cli.command()
@click.argument('project', default='.')
def init(project):
    """ プロジェクトを生成するコマンド
    """
    template_dir = os.path.join(okra.__path__[0], 'template')

    try:
        shutil.copytree(template_dir, project)
        click.echo('Created new project: {}'.format(project))
    except OSError as e:
        click.echo(e, err=True)
        sys.exit(1)


@cli.command()
@click.argument('program', default='./main.py')
def run(program):
    """ プロジェクトのプログラムを実行するコマンド
    """
    subprocess.call([sys.executable, program])


@cli.command()
@click.argument('testfile', default='./test.py')
def test(testfile):
    """ プロジェクトのテストを実行するコマンド
    """
    subprocess.call([sys.executable, testfile])

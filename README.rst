okra
========

プログラミングコンテストの解答支援ツール

Usage
-----

プロジェクトの作成::

    $ okra init [PROJECT]

プロジェクトの実行::

    $ okra run [PROGRAM]

テストの実行::

    $ okra test [TESTFILE]

プロジェクトのディレクトリ構成::

    project/
        main.py         # 解答プログラム
        test.py         # テストスクリプト
        test_1.txt      # テスト入力値ファイル
        expect_1.txt    # 期待する出力値ファイル

テスト入力値ファイル(test_*.txt)と、期待する出力値ファイル(expect_*.txt)の組を
プロジェクトのディレクトリに置くとテストケースを追加できる

Example
-------

プロジェクトの作成からプログラムの実行、テストの実行、テストを追加して実行まで::

    $ okra init p01
    Created new project: p01
    $ cd p01
    $ ls
    expect_1.txt  main.py       test.py       test_1.txt
    $ okra run
    okra   # キーボードから入力
    Hello, okra    # プログラムから出力
    $ okra test
    .
    ----------------------------------------------------------------------
    Ran 1 test in 0.053s

    OK
    $ echo world > test_2.txt
    $ echo Hello, world > expect_2.txt
    $ ls
    expect_1.txt  expect_2.txt  main.py
    test.py       test_1.txt    test_2.txt
    $ okra test
    ..
    ----------------------------------------------------------------------
    Ran 2 tests in 0.096s

    OK


Requirements
------------

- python2.7
- click (https://github.com/mitsuhiko/click)

Installation
------------

インストール方法::

    $ pip install git+https://bitbucket.org/haburibe/okra.git

TODO
----

- python2 以外の言語に対応したい
- テストの際に入力値と出力値を表示したい
- テストケースを個別に実行したい
- テストケースを生成するサブコマンドを作る
